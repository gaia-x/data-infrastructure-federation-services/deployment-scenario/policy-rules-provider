package rules.label.criterion.checkSignature

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC

ruleSetID := "LR-03"

default valid := false

valid if {
    #isValid(input)
    #Remove until new valid DIDs are created + add multiple proof vérification
    true
}

isValid(myvc) := proof if {
    #VC
    # Normalize it with compliance lab service rest api and hash it
    payload := VC.payload(myvc)

    #JWT
    # Reconstruct jwt with header + signature from jws and add payload
    # Data : JWS = Header . . Signature
    #        JWT = Header . Payload . Signature
    
    header_signature := split(myvc.proof.jws, "..")
    jwt := concat(".", [header_signature[0], payload, header_signature[1]])

    #JWK
    # Construct key structure wrt OPA built-in functions
    keys := { "keys" : [key] |
        jwk := VC.getPubKey(myvc)
        key := jwk["verificationMethod"][_]["publicKeyJwk"]
    }

    #Verify signature
    proof := io.jwt.verify_ps256(jwt, json.marshal(keys))
}