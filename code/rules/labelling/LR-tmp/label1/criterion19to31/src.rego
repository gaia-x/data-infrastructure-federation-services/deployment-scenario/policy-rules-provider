package rules.label.criterion.crit19to31

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit19to31"

default valid := false

valid if {
    core.contains(reference.complianceReferenceTitle, "EU CoC")
}