package rules.label.criterion.crit32to51_2

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit32to51_2"

default valid := false

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "ISO 27001"})

}

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "SecNumCloud"})
}

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "C5 type I"})
}

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "TISAX"})
}

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "SOC type II"})
}

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "CSA STAR Level 2"})
}