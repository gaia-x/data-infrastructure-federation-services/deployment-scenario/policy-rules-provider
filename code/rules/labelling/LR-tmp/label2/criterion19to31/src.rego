package rules.label.criterion.crit19to31_2

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit19to31_2"

default valid := false

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "EU CoC"})
}