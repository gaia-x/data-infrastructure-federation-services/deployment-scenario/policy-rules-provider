package rules.label.criterion.crit52and53_2

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit52and53_2"

default valid := false

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "SWIPO IaaS CoC"})

}

#SWIPO SaaS ??