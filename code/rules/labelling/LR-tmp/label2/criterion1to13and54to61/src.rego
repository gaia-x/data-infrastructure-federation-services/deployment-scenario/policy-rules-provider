package rules.label.criterion.crit1to13and54to61_2

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit1to13and54to61_2"

default valid := false

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "CriterionClaim"})
}