package rules.labelling.label2

import data.rules.lib.core
import data.rules.lib.verifiable
import future.keywords.every
import future.keywords.in

urlLabel[1] := "https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
urlLabel[2] := "https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/8a9bae461da3b6945ece65b342fe14093509eda4494f5b5116a14d64736211b5/data.json"
urlLabel[3] := "https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/3dddebee83fef07bc13c24537544c1ce1df1875a01a281413551c1e2b743611a/data.json"
criterionLabel(x) := { criterion | label := http.send({ "method": "GET", "url": urlLabel[x]}).body; criterion := label.credentialSubject["gax-compliance:hasRequiredCriteria"][_]["@id"] }

default isValid := false
isValid {
    false
}

#List of claims
claimsFromLSO := { claims | some claim in verifiable.getLSO().credentialSubject["gax-service:hasComplianceCertificateClaim"]; claims := core.resDid(claim["@id"]).body }

#List of self assess claims
selfAssessClaimsFromLSO := { claims | some claim in verifiable.getLSO().credentialSubject["gax-service:hasSelfAssessedComplianceCriteriaClaim"]; claims := core.resDid(claim["@id"]).body }

#List of claims from VP
claimsFromVP := { verifiable.getClaims }

#List of all claims (VP, VC)
claims := claimsFromLSO | selfAssessClaimsFromLSO | claimsFromVP

#List of scheme
schemesFromClaims := { schemes | some claim in claims; schemes := core.resDid(claim["gax-compliance:hasComplianceCertificationScheme"]["@id"]).body }

#List of criterion from all scheme
criterionID := { criterion | some scheme in schemesFromClaims; some criteria in scheme["gax-compliance:grantsComplianceCriteria"]; criterion := criteria["@id"]}

#List of criterion value from all scheme
criterionValue := { criterion | some criteria in criterionID; criterion := core.resDid(criteria).body}

#List of criterion for label x
criterionFromLSO(x) := { criterion | some criteria in criterionValue; criteria["gax-compliance:hasLevel"]["@value"] == concat("", ["Level ", format_int(x, 10)]); criterion := criteria["@id"]}

#TODO Find a better way to check criterion
isAllCriterionOk(x) {
    count(criterionLabel(x) - criterionFromLSO(x)) == 0
}

default isCriterionPres(id, level) := false
isCriterionPres(id, level) {
    core.contains(criterionFromLSO(level), id)
}

listForLabel(x) := { res | some criteria in criterionLabel(x); res := [core.resDid(criteria).body["gax-compliance:hasName"]["@value"], isCriterionPres(criteria, x)]}

listForLabel1 := { res | some criteria in criterionLabel(1); res := [core.resDid(criteria).body["gax-compliance:hasName"]["@value"], isCriterionPres(criteria, 1)]}