package rules.label.label2.crit1to13and54to61

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit1to13and54to61"

default valid := false

valid if {
    core.contains(reference.checkThirdPartyCompliance, {true, "CriterionClaim"})
}