package rules.labelling.label

import future.keywords.every
import data.rules.lib.core
import data.rules.lib.VC
import data.rules.lib.VPLSO

ruleSetLabelLevel[0] := ["LR-01"]
ruleSetLabelLevel[1] := ["crit1to13and54to61", "crit19to31", "crit19to31and54", "crit32to51", "crit52and53"]
ruleSetLabelLevel[2] := ["crit1to13and54to61_2", "crit19to31_2", "crit19to31and54_2", "crit32to51_2", "crit52and53_2"]
ruleSetLabelLevel[3] := ["crit1to13and54to61_2", "crit19to31_2", "crit19to31and54_2", "crit32to51_2", "crit52and53_2"]

lstValidcriterionLabel(level) = {i | some i in ruleSetLabelLevel[level]; some j in data.rules.label.criterion; j.ruleSetID == i; j.valid == true}
lstInvalidcriterionLabel(level) = {i | some i in ruleSetLabelLevel[level]; some j in data.rules.label.criterion; j.ruleSetID == i; j.valid == false}

default isValid := false
isValid {
    label0
}

lstNonValid := { rule | data.rules.labelling.criterion[rule].valid == false}

valid := res {
    isValid

    res := {"valid": "true",
            "VC": {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1"
                      ],
                    "@id": "",
                    "@type": [
                      "VerifiableCredential"
                    ],
                    "issuer": "",
                    "expirationDate": "",
                    "credentialSubject": {
                      "@type": [
                        "labelling"
                      ],
                      "label": count(labelTst3),
                      "id": vc,
                      "hashParticipant": ""
                    },
                    "evidence": {
                      "verifier": "",
                      "type": [],
                      "subjectPresent": "digital",
                      "documentPresent": "digital"
                    }
                  }}
}

valid := res {
    not isValid
    res := {"valid": "false", "lstNonValid": lstInvalidcriterionLabel(0)}
}

vc := res {
    res := VPLSO.getLocatedServiceVC.credentialSubject["gax-service:isImplementationOf"]["@id"]
}

default label0 := false
label0 {
    k = lstValidcriterionLabel(0)
    count(k) == count(ruleSetLabelLevel[0])
}

ruleSetLabelLevelTest[1] := array.concat(ruleSetLabelLevel[0], ruleSetLabelLevel[1])
ruleSetLabelLevelTest[2] := array.concat(array.concat(ruleSetLabelLevel[0], ruleSetLabelLevel[1]), ruleSetLabelLevel[2])
ruleSetLabelLevelTest[3] := array.concat(array.concat(array.concat(ruleSetLabelLevel[0], ruleSetLabelLevel[1]), ruleSetLabelLevel[2]), ruleSetLabelLevel[3])

lstValidcriterionLabelTest(level) = {i | some i in ruleSetLabelLevelTest[level]; some j in data.rules.label.criterion; j.ruleSetID == i; j.valid == true}

testMockLabel := ["crit1to13and54to61", "crit19to31", "crit19to31and54", "crit52and53"]

#Why not mais pas fou...
labelTst3[iterat] {
    k := lstValidcriterionLabelTest(iterat)
    count(k) == count(ruleSetLabelLevelTest[iterat])
}

labelTst5 := res {
    res := count(labelTst3)
}

##############################################################################################################################
#Nouvelle implementation avec les 61 criterion pour chaque label
##############################################################################################################################

criterionLabel[0] := { crit | data.rules.label.label0[crit].ruleSetID }
criterionLabel[1] := { crit | data.rules.label.label1[crit].ruleSetID }
criterionLabel[2] := { crit | data.rules.label.label2[crit].ruleSetID }
criterionLabel[3] := { crit | data.rules.label.label3[crit].ruleSetID }

criterionLabelConcat[1] := criterionLabel[1]
#criterionLabelConcat[2] := array.concat(criterionLabel[1], criterionLabel[2])
#criterionLabelConcat[3] := array.concat(array.concat(criterionLabel[1], criterionLabel[2]), criterionLabel[3])

lstValidCriterionForLabel(level) = {i | some i in criterionLabel[level]; some j in data.rules.label[concat("", ["label", format_int(level, 10)])]; j.ruleSetID == i; j.valid == true}
lstInvalidCriterionForLabel(level) = {i | some i in criterionLabel[level]; some j in data.rules.label[concat("", ["label", format_int(level, 10)])]; j.ruleSetID == i; j.valid == false}

#Temporaire
lstValidCriterionForLabel1 = {i | some i in criterionLabel[1]; some j in data.rules.label.label1; j.ruleSetID == i; j.valid == true}
lstValidCriterionForLabel2 = {i | some i in criterionLabel[2]; some j in data.rules.label.label2; j.ruleSetID == i; j.valid == true}
lstValidCriterionForLabel3 = {i | some i in criterionLabel[3]; some j in data.rules.label.label3; j.ruleSetID == i; j.valid == true}

#label[iterat] {
#    k := lstValidCriterionForLabel(iterat)
#    count(k) == count(criterionLabelConcat[iterat])
#}

##############################################################################################################################
#Autre implementation avec les 61 criterion pour chaque label
##############################################################################################################################

criterion := ["crit32to51"]
crieterionForLabel[0] := {"label": 0, "criterion": []}
crieterionForLabel[1] := {"label": 1, "criterion": criterion}
crieterionForLabel[2] := {"label": 2, "criterion": criterion}
crieterionForLabel[3] := {"label": 3, "criterion": criterion}

lstValidCriterionForLevel(level) = {i | some i in crieterionForLabel[level].criterion; some j in data.rules.label[concat("", ["label", format_int(level, 10)])]; j.ruleSetID == i; j.valid == true}

labelxtst := res {
    some it in crieterionForLabel
    print(it)
    res := (count(it.criterion) == count(lstValidCriterionForLevel(it.label)))
}

testlabel[iterat] {
    k := lstValidCriterionForLevel(iterat)
    count(k) == count(crieterionForLabel[iterat].criterion)
}
