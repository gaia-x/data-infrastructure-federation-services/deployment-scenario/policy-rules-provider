package rules.label.label1.crit19to31and54

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit19to31and54"

default valid := false

valid if {
    core.contains(reference.complianceReferenceTitle, "CISPE Data Protection CoC")
}