package rules.label.label1.crit32to51

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit32to51"

default valid := false

valid if {
    core.contains(reference.complianceReferenceTitle, "ISO 27001")
}

valid if {
    core.contains(reference.complianceReferenceTitle, "SecNumCloud")
}

valid if {
    core.contains(reference.complianceReferenceTitle, "C5 type I")
}

valid if {
    core.contains(reference.complianceReferenceTitle, "TISAX")
}

valid if {
    core.contains(reference.complianceReferenceTitle, "SOC type II")
}

valid if {
    core.contains(reference.complianceReferenceTitle, "CSA STAR Level 2")
}