package rules.label.label1.crit52and53

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.reference

ruleSetID := "crit52and53"

default valid := false

valid if {
    core.contains(reference.complianceReferenceTitle, "SWIPO IaaS CoC")
}

#SWIPO SaaS ??