package rules.verifydataproof.policy

import future.keywords.if

default allow := false


allow if {
    #Get Verifiable Credential (for instance from data repo)
	vc := data.vcs.vc1
    vc_no_proof := json.remove(vc, ["proof"])

    #Normalize it with compliance lab service rest api
    return := http.send({"method": "POST", "url": "https://compliance.gaia-x.eu/v2204/api/normalize", "headers": {"Content-Type": "application/json"}, "body": vc_no_proof })
    #Hash normalization to get paylaod
    payload := crypto.sha256(return.raw_body)
    
    #Reconstruct jwt with header + signature from jws and add payload
    #Data : JWS = Header . . Signature
    #       JWT = Header . Payload . Signature
    jws := vc["proof"]["jws"]
    header_signature := split(jws, "..")
    jwt := concat(".", [header_signature[0], payload, header_signature[1]])

    #Get JWK (for instance from data repo)
    pub_keys := { key |
        jwk := data.pubkeys.key1
        key := jwk["verificationMethod"][_]["publicKeyJwk"]
    }
	keys := {
		"keys": pub_keys
	}

    #Verify Signature
    io.jwt.verify_ps256(jwt, json.marshal(keys))
}