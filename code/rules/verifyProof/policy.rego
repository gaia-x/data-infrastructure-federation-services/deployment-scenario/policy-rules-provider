package rules.verifyproof.policy

import future.keywords.if

default allow := false


allow if {
    verify_proof(input.vc)
}

verify_proof(vc) := proof if {
    #VC
    # Normalize it with compliance lab service rest api and hash it
    vcnp_normalized := normalize_vc(vc)
    payload := crypto.sha256(vcnp_normalized)

    #JWT
    # Reconstruct jwt with header + signature from jws and add payload
    # Data : JWS = Header . . Signature
    #        JWT = Header . Payload . Signature
    jws := vc["proof"]["jws"]
    header_signature := split(jws, "..")
    jwt := concat(".", [header_signature[0], payload, header_signature[1]])

    #JWK
    # Construct key structure wrt OPA built-in functions
    pub_keys := { key |
        jwk := get_key(vc)
        key := jwk["verificationMethod"][_]["publicKeyJwk"]
    }
	keys := {
		"keys": pub_keys
	}
    
    #Verify signature
    proof := io.jwt.verify_ps256(jwt, json.marshal(keys))
}

normalize_vc(vc) := vcnp if {
    vc_no_proof := json.remove(vc, ["proof"])
    #Normalize it with compliance lab service rest api
    return := http.send({"method": "POST", "url": "https://compliance.gaia-x.eu/v2204/api/normalize", "headers": {"Content-Type": "application/json"}, "body": vc_no_proof })
    #Hash normalization to get paylaod
    vcnp := return.raw_body
}

get_key(vc) := key if {
    #Get domain name from proof of vc
    vf := vc["proof"]["verificationMethod"]
    dn := trim(vf, "did:web:")
    url := concat("", ["https://", dn, "/.well-known/did.json"])
    #Call vc issuer service to get its pub key
    return := http.send({"method": "GET", "url": url, "headers": {"Content-Type": "application/json"} })
    key := json.unmarshal(return["raw_body"])
}
