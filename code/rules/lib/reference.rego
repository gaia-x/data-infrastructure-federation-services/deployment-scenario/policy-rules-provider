package rules.lib.reference

import future.keywords.every
import future.keywords.if
import data.rules.lib.VPLSO
import data.rules.lib.core

complianceReferenceTitle[title] {
    some it in VPLSO.getLocatedServiceVC.credentialSubject["gax-compliance:hasComplianceCertificateClaim"]
    resFromComplianceCertificateClaim := core.resDid(it["@id"]).body["gax-compliance:hasComplianceCertificationScheme"]["@id"]
    resFromComplianceCertificationScheme := core.resDid(resFromComplianceCertificateClaim).body["gax-compliance:hasComplianceReference"]["@id"]
    title := core.resDid(resFromComplianceCertificationScheme).body["gax-compliance:hasComplianceReferenceTitle"]["@value"]
}

checkThirdPartyCompliance[compliance] {
    some it in VPLSO.getLocatedServiceVC.credentialSubject["gax-compliance:hasComplianceCertificateClaim"]
    resFromComplianceCertificateClaim := core.resDid(it["@id"]).body["gax-compliance:hasComplianceCertificationScheme"]["@id"]
    resFromComplianceCertificationScheme := core.resDid(resFromComplianceCertificateClaim).body["gax-compliance:hasComplianceReference"]["@id"]
    title := core.resDid(resFromComplianceCertificationScheme).body["gax-compliance:hasComplianceReferenceTitle"]["@value"]
    isThirdParty := core.contains(core.resDid(it["@id"]).body["@type"], "ThirdPartyComplianceCertificateClaim")
    compliance := {title, isThirdParty}
}

complianceReference[complianceReference] {
    some it in VPLSO.getLocatedServiceVC.credentialSubject["gax-compliance:hasComplianceCertificateClaim"]
    resFromComplianceCertificateClaim := core.resDid(it["@id"]).body["gax-compliance:hasComplianceCertificationScheme"]["@id"]
    resFromComplianceCertificationScheme := core.resDid(resFromComplianceCertificateClaim).body["gax-compliance:hasComplianceReference"]["@id"]
    complianceReference := core.resDid(resFromComplianceCertificationScheme).body.hasComplianceCertificationSchemes
}
