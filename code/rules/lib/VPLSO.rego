package rules.lib.VPLSO
#VPLSO -> Verifiable Presentation Located Service Offering

import future.keywords.if
import future.keywords.every
import data.rules.lib.core

default is_VPLSO := false

is_VPLSO if {
    core.has_field(resource,"@type")
    core.has_field(resource,"verifiableCredential")
    core.has_field(resource,"proof")
    core.contains(resource["@type"], "verifiablePresentation")
}

is_VPincludedinInput if {
    core.has_field(input,"vp")
    core.has_field(input.vc,"@type")
}

resource = input.vp if {
	is_VPincludedinInput
}

resource = input if {
	not is_VPincludedinInput
}

serviceOffering := service {
    serviceDid := getLocatedServiceVC.credentialSubject["gax-service:isImplementationOf"]["@id"]
    service := core.resDid(serviceDid).body
}

getLocatedServiceVC := res {
    some it in resource.verifiableCredential
    core.contains(it["@type"], "VerifiableCredential")
    isLSO(it)
    res := it
}

default isLSO(lso) := false

isLSO(lso) {
    core.contains(lso.credentialSubject["@type"], "gax-service:LocatedServiceOffering")
}

isLSO(lso) {
    lso.credentialSubject["@type"] == "gax-service:LocatedServiceOffering"
}