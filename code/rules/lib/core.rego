package rules.lib.core

format(msg) := obj {obj :=  {"msg": msg} }

format_with_id(msg, id) := msg_fmt {
	msg_fmt := {
		"msg": sprintf("%s: %s", [id, msg]),
		"details": {"policyID": id}
	}
}

default has_field(obj, field) := false
has_field(obj, field) {
	not object.get(obj, field, "N_DEFINED") == "N_DEFINED"
}

missing_field(obj, field) {
	obj[field] == ""
}

missing_field(obj, field) {
	not has_field(obj, field)
}

default contains(items, elem) := false
contains(items, elem) {
  items[_] == elem
}
contains(items, elem) {
  items == elem
}

resDid(did) := res {
    url := concat("", ["https://", replace(trim_prefix(did, "did:web:"), ":", "/")])
    res := http.send({ "method": "GET", "url": url})
}