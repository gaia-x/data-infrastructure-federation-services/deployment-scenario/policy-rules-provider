package rules.lib.verifiable

import future.keywords.if
import future.keywords.every
import data.rules.lib.core

##################################################
############## Verfiable credential ##############
##################################################

url_normalize := "https://compliance.gaia-x.eu/v2204/api/normalize"

default isVC(vc) := false
isVC(vc) if {
    core.has_field(vc,"@type")
    core.has_field(vc,"credentialSubject")
    core.has_field(vc,"proof")
    core.contains(vc["@type"], "VerifiableCredential")
}

normalize(vc) = vcnp if {
    vc_no_proof := json.remove(vc, ["proof"])
    #Normalize it with compliance lab service rest api
    return := http.send({"method": "POST", "url": url_normalize , "headers": {"Content-Type": "application/json"}, "body": vc_no_proof })
    #Hash normalization to get paylaod
    vcnp := return.raw_body
}

payload(vc) = payloadvc if {
    vcnp_normalized := normalize(vc)
    payloadvc := crypto.sha256(vcnp_normalized)
}


getPubKey(vc) = key if {
    #Get domain name from proof of vc
    vf := vc["proof"]["verificationMethod"]
    dn := trim(vf, "did:web:")
    url := concat("", ["https://", dn, "/.well-known/did.json"])
    #Call vc issuer service to get its pub key
    return := http.send({"method": "GET", "url": url, "headers": {"Content-Type": "application/json"} })
    key := json.unmarshal(return["raw_body"])
}

##################################################
############# Verfiable presentation #############
##################################################

default isVP(vp) := false
isVP(vp) if {
    core.has_field(vp,"@type")
    core.has_field(vp,"verifiableCredential")
    core.has_field(vp,"proof")
    core.contains(vp["@type"], "VerifiablePresentation")
}

getServiceOffering := service {
    serviceDid := getLSO.credentialSubject["gax-service:isImplementationOf"]["@id"]
    service := core.resDid(serviceDid).body
}

getLSO := res {
    isVP(input)
    some it in input.verifiableCredential
    core.contains(it["@type"], "VerifiableCredential")
    isLSO(it)
    res := it
}

default isLSO(vp) := false
isLSO(vp) {
    core.contains(vp.credentialSubject["@type"], "gax-service:LocatedServiceOffering")
}
isLSO(vp) {
    vp.credentialSubject["@type"] == "gax-service:LocatedServiceOffering"
}
isLSO(vp) {
    core.contains(vp.credentialSubject["@type"], "LocatedServiceOffering")
}
isLSO(vp) {
    vp.credentialSubject["@type"] == "LocatedServiceOffering"
}

getClaims := [claims] {
    isVP(input)
    some it in input.verifiableCredential
    isClaim(it.credentialSubject)
    claims := it.credentialSubject
}

default isClaim(claim) := false
#ThirsPartyCompliance Claim
isClaim(claim) {
    core.contains(claim["@type"], "gax-compliance:ThirdPartyComplianceCertificateClaim")
}
isClaim(claim) {
    claim["@type"] == "gax-compliance:ThirdPartyComplianceCertificateClaim"
}
#Self Assessed Compliance Criteria Claim
isClaim(claim) {
    core.contains(claim["@type"], "gax-compliance:SelfAssessedComplianceCriteriaClaim")
}
isClaim(claim) {
    claim["@type"] == "gax-compliance:SelfAssessedComplianceCriteriaClaim"
}
#Compliance Certificate Claim
isClaim(claim) {
    core.contains(claim["@type"], "gax-compliance:ComplianceCertificateClaim")
}
isClaim(claim) {
    claim["@type"] == "gax-compliance:ComplianceCertificateClaim"
}