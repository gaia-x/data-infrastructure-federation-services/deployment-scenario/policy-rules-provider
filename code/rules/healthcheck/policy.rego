# Rule to check the Policyrule component is working
package rules.healthcheck.policy

import future.keywords.if

default hello := false

# return true if input is world 
hello if input.message == "world"
