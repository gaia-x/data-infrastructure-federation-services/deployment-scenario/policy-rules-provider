package rules.compliance.participant.CPR09_checkDid

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC

ruleSetID := "CPR-09"

default valid := false

valid if {
    #not isDIDOk(input)
    #Remove until new valid DIDs are created
    true
}

regexDID := "^did:web:[a-z0-9A-Z].[a-z0-9A-Z]"

isDIDOk(myvc) if {
    count(lstNonValidDIDinVC(myvc)) > 0
}

lstDIDinVC(myvc) := { did | [path, value] := walk(myvc) ;did := value[_]; regex.match(regexDID, did)}
lstValidDIDinVC(myvc) := { did | response := http.send({ "method": "GET", "url": concat("", ["http://resolver.lab.gaia-x.eu:8080/1.0/identifiers/", lstDIDinVC(myvc)[did]])}); response.status_code == 200}
lstNonValidDIDinVC(myvc) := lstDIDinVC(myvc) - lstValidDIDinVC(myvc)

lstDID := lstDIDinVC(input)
lstValidDID := lstValidDIDinVC(input)
lstNonValidDID := lstDID - lstValidDID