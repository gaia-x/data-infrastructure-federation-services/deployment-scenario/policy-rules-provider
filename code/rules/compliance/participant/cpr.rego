package rules.compliance.complianceParticipant

import data.rules.lib.VC

anyNotValid {
    data.rules.compliance.participant[_].valid == false
}

default isValid := false
isValid {
    not anyNotValid
}

lstNonValid := { rule | data.rules.compliance.participant[rule].valid == false}
lstValid := { rule | data.rules.compliance.participant[rule].valid == true}

valid := res {
    isValid

    res := {"valid": "true",
            "VC": {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1",
                        {
                            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        }
                    ],
                    "@id": "",
                    "@type": [
                      "VerifiableCredential"
                    ],
                    "issuer": "",
                    "expirationDate": "",
                    "credentialSubject": {
                      "@type": "gax-compliance:ComplianceObject",
                      "targetType" : "Participant",
                      "vc": vc,
                      "validRules": lstValid,
                      "vcProof": vcProof
                    },
                    "evidence": {
                      "verifier": "",
                      "type": [],
                      "subjectPresent": "digital",
                      "documentPresent": "digital"
                    }
                  }}
}

valid := res {
    not isValid
    res := {"valid": "false", "lstNonValid": lstNonValid}
}

vc := res {
    res := input.credentialSubject["@id"]
}

vc := res {
    res := input.credentialSubject.id
}

vcProof := res {
    res := input.proof
}