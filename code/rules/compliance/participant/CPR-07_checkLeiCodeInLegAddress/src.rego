package rules.compliance.participant.CPR07_checkLeiCodeInLegAddress

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC
import future.keywords.in

ruleSetID := "CPR-07"

default valid := false

valid if {
    isLeiCodeInLegAddressValid(input)
}

isLeiCodeInLegAddressValid(myvc) if {
    leiCode := input["credentialSubject"]["gax-participant:leiCode"]["@value"]
    response := http.send({ "method": "GET", "url": concat("", ["https://api.gleif.org/api/v1/lei-records?filter[lei]=", leiCode])})
    body := json.unmarshal(response["raw_body"])
    leiCountry := body.data[0].attributes.entity.legalAddress.country

    country := input["credentialSubject"]["gax-participant:legalAddress"]["vcard:country-name"]["@value"]
    result := leiCountry == country
}


