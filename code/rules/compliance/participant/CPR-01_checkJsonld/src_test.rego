package rules.compliance.participant.CPR01_checkJsonld

import future.keywords.if
import data.rules.lib.core

__inputTest := { "vc" : {
        "@context": [
            ""
        ],
        "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/did:web:compliance4tf.byo-networks.net/vc/0b6a374a677829b131a5b30edeeba550427aad79e5eab7c3bf9aa854ac4e4166/data.json",
        "@type": [
            "VerifiableCredential",
            "LegalPerson"
        ],
        "credentialSubject": {
            "@context": [],
            "@type": [
                "c4tf-service:LocatedServiceOffering"
            ],
            "c4tf-service:availableOn": {
                "@context": [],
                "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/location/02a48ae34f3ccef57b92dde156a5e82b0b64f01f1bd5309763373fa60a4532fb/data.json",
                "@type": [
                    "c4tf-participant:Location"
                ]
            },
            "c4tf-service:isLocalizedInstanceOf": {
                "@context": [],
                "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/stataic/service/c6be7c0de8560f6e34279490b03f60b47d68e882240537da2a477dde7988faa5/data.json",
                "@type": [
                    "c4tf-service:ServiceOffering"
                ]
            },
            "c4tf-compliance:ComplianceResourceClaims": [
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/f836156bbaf1d3a10b47bf1e7894d412ae03e32ad4339146ded461b79b14b159/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/ca1621056aa4bb93efb70d900c7e325cdc02f6dc7f8b1a6c1aa2d30aa5beb348/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/31b9b9f0907e03a638c6754ce12bf5b583c6dbe196586cea927ada640cbddb1b/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/ebe0c87620814c3eb1666a2b8dc154a63eb6abdb2ff31a59806a312c98c8ce2f/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/7a256fe40b9cd69a02001fec8ced3a7f60e12f2cbd2b9a8e9b40ea318adaf1ed/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                }
            ],
            "c4tf-compliance:ComplianceLabelClaims": [],
            "c4tf-compliance:AssertedComplianceRessourceClaims": [],
            "c4tf-compliance:AssertedComplianceLabelClaims": [],
            "id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/locatedservice/813a23887804f678f88d59286114b2d21a8d1787027d8ee7361a9638b9592d08/data.json",
            "c4tf-service:designation": "RBX4--HPC for Nutanix"
        },
        "proof": {
            "type": "JsonWebKey2020",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:compliance4tf.byo-networks.net",
            "created": "2022-08-26T09:36:20.800599+00:00",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..a4q5yjNraGHmT_I_QIpwuY5UCZeseQZrE1B6i-lmaAb4i5tJq8uxVCe-rKhnwD1bpGX8IxWn7LsZ30wPLygadcuHmL9iqPMPJ5nWjSSvV1diPX96TkzmBCjQHIECHsTfRRiJjvQRsE8fJSrRkFs-ChMTyJ82NK9MxZXW2ixiPXMGLKwZnvK8p2BU96j-s1SOS8z122AEkOeEKOK4yIFAXXVAK7VbqbtfrkhJQrGER1e7dcb--3KPKycw-mkU5-Uu5ynWkdp-zxGCiMWt8m_P6H3-nZeIi4JCpaXjVw9r4FFcPJ661dqYb281Ft9-EDAk3NaoXo5keoDg3PYtxCJoZbiy1P7sXH-bs9PEazjwVwH-NL6xoOFYd2LAKpNulOpPdbiVnMzXQanDXWT_m2F8UgN510kRfLqKC7_SEfOFNG6eBkzE42MGB7USzBS0Gk8XTpJDxjf65FMOQlb10s6pvBUu0qVEAdShAIikLvqp_tJ_Iygq1ywXoCAzBY98R8PFEvGFyP0tEA79slVlKAVMMO_zVOcvNjowHO6MerT1DrbBkGaoZ_Iql3XptOh_3RBVew3aRbE85yoecwvNJAqvDjJrbWCKDeYPH1DwhCZVRLeIBdtP_avg_nogs3I77FCoLATNTfNQY_4-7yOEjhc7OO98Gwj8-f_6P_MyvqgmlPI",
            "jwt": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.6a4d329d81a18a6d8c62dd582c0c0c35d93c4dd0cae4a6021deb498ec5fd2b04.a4q5yjNraGHmT_I_QIpwuY5UCZeseQZrE1B6i-lmaAb4i5tJq8uxVCe-rKhnwD1bpGX8IxWn7LsZ30wPLygadcuHmL9iqPMPJ5nWjSSvV1diPX96TkzmBCjQHIECHsTfRRiJjvQRsE8fJSrRkFs-ChMTyJ82NK9MxZXW2ixiPXMGLKwZnvK8p2BU96j-s1SOS8z122AEkOeEKOK4yIFAXXVAK7VbqbtfrkhJQrGER1e7dcb--3KPKycw-mkU5-Uu5ynWkdp-zxGCiMWt8m_P6H3-nZeIi4JCpaXjVw9r4FFcPJ661dqYb281Ft9-EDAk3NaoXo5keoDg3PYtxCJoZbiy1P7sXH-bs9PEazjwVwH-NL6xoOFYd2LAKpNulOpPdbiVnMzXQanDXWT_m2F8UgN510kRfLqKC7_SEfOFNG6eBkzE42MGB7USzBS0Gk8XTpJDxjf65FMOQlb10s6pvBUu0qVEAdShAIikLvqp_tJ_Iygq1ywXoCAzBY98R8PFEvGFyP0tEA79slVlKAVMMO_zVOcvNjowHO6MerT1DrbBkGaoZ_Iql3XptOh_3RBVew3aRbE85yoecwvNJAqvDjJrbWCKDeYPH1DwhCZVRLeIBdtP_avg_nogs3I77FCoLATNTfNQY_4-7yOEjhc7OO98Gwj8-f_6P_MyvqgmlPI",
            "sha256": "6a4d329d81a18a6d8c62dd582c0c0c35d93c4dd0cae4a6021deb498ec5fd2b04"
        },
        "id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/did:web:compliance4tf.byo-networks.net/vc/0b6a374a677829b131a5b30edeeba550427aad79e5eab7c3bf9aa854ac4e4166/data.json"
    }
}

__inputTestIssue := { "vc" : {
        "context": [
            ""
        ],
        "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/did:web:compliance4tf.byo-networks.net/vc/0b6a374a677829b131a5b30edeeba550427aad79e5eab7c3bf9aa854ac4e4166/data.json",
        "@type": [
            "VerifiableCredential",
            "LegalPerson"
        ],
        "credentialSubject": {
            "@context": [],
            "@type": [
                "c4tf-service:LocatedServiceOffering"
            ],
            "c4tf-service:availableOn": {
                "@context": [],
                "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/location/02a48ae34f3ccef57b92dde156a5e82b0b64f01f1bd5309763373fa60a4532fb/data.json",
                "@type": [
                    "c4tf-participant:Location"
                ]
            },
            "c4tf-service:isLocalizedInstanceOf": {
                "@context": [],
                "@id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/stataic/service/c6be7c0de8560f6e34279490b03f60b47d68e882240537da2a477dde7988faa5/data.json",
                "@type": [
                    "c4tf-service:ServiceOffering"
                ]
            },
            "c4tf-compliance:ComplianceResourceClaims": [
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/f836156bbaf1d3a10b47bf1e7894d412ae03e32ad4339146ded461b79b14b159/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/ca1621056aa4bb93efb70d900c7e325cdc02f6dc7f8b1a6c1aa2d30aa5beb348/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/31b9b9f0907e03a638c6754ce12bf5b583c6dbe196586cea927ada640cbddb1b/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/ebe0c87620814c3eb1666a2b8dc154a63eb6abdb2ff31a59806a312c98c8ce2f/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                },
                {
                    "comply_with": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/static/compliance_resource/7a256fe40b9cd69a02001fec8ced3a7f60e12f2cbd2b9a8e9b40ea318adaf1ed/data.json",
                    "certified_by": "https://compliance4tf.byo-networks.net/tf/participant/6a3ce0590dce3815d0f910002cb5a095b175e018393f83789450c4560da49555/certifier/36095634ea00dd8a3de277f93903e3bfe00b2002bd01a4b76b811e1cca259755/data.json"
                }
            ],
            "c4tf-compliance:ComplianceLabelClaims": [],
            "c4tf-compliance:AssertedComplianceRessourceClaims": [],
            "c4tf-compliance:AssertedComplianceLabelClaims": [],
            "id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/locatedservice/813a23887804f678f88d59286114b2d21a8d1787027d8ee7361a9638b9592d08/data.json",
            "c4tf-service:designation": "RBX4--HPC for Nutanix"
        },
        "proof": {
            "type": "JsonWebKey2020",
            "proofPurpose": "assertionMethod",
            "verificationMethod": "did:web:compliance4tf.byo-networks.net",
            "created": "2022-08-26T09:36:20.800599+00:00",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..a4q5yjNraGHmT_I_QIpwuY5UCZeseQZrE1B6i-lmaAb4i5tJq8uxVCe-rKhnwD1bpGX8IxWn7LsZ30wPLygadcuHmL9iqPMPJ5nWjSSvV1diPX96TkzmBCjQHIECHsTfRRiJjvQRsE8fJSrRkFs-ChMTyJ82NK9MxZXW2ixiPXMGLKwZnvK8p2BU96j-s1SOS8z122AEkOeEKOK4yIFAXXVAK7VbqbtfrkhJQrGER1e7dcb--3KPKycw-mkU5-Uu5ynWkdp-zxGCiMWt8m_P6H3-nZeIi4JCpaXjVw9r4FFcPJ661dqYb281Ft9-EDAk3NaoXo5keoDg3PYtxCJoZbiy1P7sXH-bs9PEazjwVwH-NL6xoOFYd2LAKpNulOpPdbiVnMzXQanDXWT_m2F8UgN510kRfLqKC7_SEfOFNG6eBkzE42MGB7USzBS0Gk8XTpJDxjf65FMOQlb10s6pvBUu0qVEAdShAIikLvqp_tJ_Iygq1ywXoCAzBY98R8PFEvGFyP0tEA79slVlKAVMMO_zVOcvNjowHO6MerT1DrbBkGaoZ_Iql3XptOh_3RBVew3aRbE85yoecwvNJAqvDjJrbWCKDeYPH1DwhCZVRLeIBdtP_avg_nogs3I77FCoLATNTfNQY_4-7yOEjhc7OO98Gwj8-f_6P_MyvqgmlPI",
            "jwt": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.6a4d329d81a18a6d8c62dd582c0c0c35d93c4dd0cae4a6021deb498ec5fd2b04.a4q5yjNraGHmT_I_QIpwuY5UCZeseQZrE1B6i-lmaAb4i5tJq8uxVCe-rKhnwD1bpGX8IxWn7LsZ30wPLygadcuHmL9iqPMPJ5nWjSSvV1diPX96TkzmBCjQHIECHsTfRRiJjvQRsE8fJSrRkFs-ChMTyJ82NK9MxZXW2ixiPXMGLKwZnvK8p2BU96j-s1SOS8z122AEkOeEKOK4yIFAXXVAK7VbqbtfrkhJQrGER1e7dcb--3KPKycw-mkU5-Uu5ynWkdp-zxGCiMWt8m_P6H3-nZeIi4JCpaXjVw9r4FFcPJ661dqYb281Ft9-EDAk3NaoXo5keoDg3PYtxCJoZbiy1P7sXH-bs9PEazjwVwH-NL6xoOFYd2LAKpNulOpPdbiVnMzXQanDXWT_m2F8UgN510kRfLqKC7_SEfOFNG6eBkzE42MGB7USzBS0Gk8XTpJDxjf65FMOQlb10s6pvBUu0qVEAdShAIikLvqp_tJ_Iygq1ywXoCAzBY98R8PFEvGFyP0tEA79slVlKAVMMO_zVOcvNjowHO6MerT1DrbBkGaoZ_Iql3XptOh_3RBVew3aRbE85yoecwvNJAqvDjJrbWCKDeYPH1DwhCZVRLeIBdtP_avg_nogs3I77FCoLATNTfNQY_4-7yOEjhc7OO98Gwj8-f_6P_MyvqgmlPI",
            "sha256": "6a4d329d81a18a6d8c62dd582c0c0c35d93c4dd0cae4a6021deb498ec5fd2b04"
        },
        "id": "https://compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce/static/did:web:compliance4tf.byo-networks.net/vc/0b6a374a677829b131a5b30edeeba550427aad79e5eab7c3bf9aa854ac4e4166/data.json"
    }
}

test_checkJsonldValid if {
    valid with input as __inputTest
}

test_checkJsonldValid_deny if {
    not valid with input as __inputTestIssue
}