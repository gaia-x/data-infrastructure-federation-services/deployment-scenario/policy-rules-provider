package rules.compliance.participant.CPR01_checkJsonld

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC

ruleSetID := "CPR-01"

default valid := false

valid if {
    isJsonld(input)
}

isJsonld(myvc) if {
    context := "@context"
    myvc[context]
}