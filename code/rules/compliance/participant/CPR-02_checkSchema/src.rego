package rules.compliance.participant.CPR02_checkSchema

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CPR-02"

default valid := false

valid if {
    #isSchemaValid
    true
}

isSchemaValid {
    verifiable.isVC(input)
    response := http.send({ "method": "POST", "url": "https://schemavalidator.abc-federation.gaia-x.community/participant", "body": input});
    response.status_code == 200
}