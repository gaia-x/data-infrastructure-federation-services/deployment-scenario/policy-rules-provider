package rules.compliance.participant.CPR02_checkSchema

import future.keywords.if
import data.rules.lib.core

__inputTest := { "vc" : {
                          "@context": [
                            "https://www.w3.org/2018/credentials/#",
                            {
                              "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
                              "xsd": "http://www.w3.org/2001/XMLSchema#",
                              "vcard": "http://www.w3.org/2006/vcard/ns#",
                              "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
                              "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                            }
                          ],
                          "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/provider.json",
                          "@type": [
                            "VerifiableCredential"
                          ],
                          "issuer": "did:web:abc-federation.gaia-x.community",
                          "issuanceDate": "2022-09-23T23:23:23.235Z",
                          "credentialSubject": {
                            "@id": "did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/data.json",
                            "@type": [
                              "gax-participant:Provider"
                            ],
                            "gax-participant:hasLocations": [
                              {
                                "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/09.json",
                                "@type": "gax-participant:Location"
                              },
                              {
                                "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/12.json",
                                "@type": "gax-participant:Location"
                              },
                              {
                                "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/location/13.json",
                                "@type": "gax-participant:Location"
                              }
                            ],
                            "gax-participant:hasDidWeb": {
                              "@value": "did:web:ovh.provider.gaia-x.community",
                              "@type": "xsd:string"
                            },
                            "gax-participant:hasServiceOffering": [
                              {
                                "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/service-offering/S-01.json",
                                "@type": "gax-service:ServiceOffering"
                              },
                              {
                                "@id": "did:web:ovh.provider.gaia-x.community/public/json-ld/service-offering/S-02.json",
                                "@type": "gax-service:ServiceOffering"
                              }
                            ],
                            "gax-participant:registrationNumber": {
                              "@value": "493376008",
                              "@type": "xsd:string"
                            },
                            "gax-participant:legalAddress": {
                              "@type": "vcard:Address",
                              "vcard:country-name": {
                                "@value": "FR",
                                "@type": "xsd:string"
                              },
                              "vcard:street-address": {
                                "@value": "2 Rue Kellermann",
                                "@type": "xsd:string"
                              },
                              "vcard:postal-code": {
                                "@value": "59100",
                                "@type": "xsd:string"
                              },
                              "vcard:locality": {
                                "@value": "Roubaix",
                                "@type": "xsd:string"
                              }
                            },
                            "gax-participant:headquarterAddress": {
                              "@type": "vcard:Address",
                              "vcard:country-name": {
                                "@value": "FR",
                                "@type": "xsd:string"
                              },
                              "vcard:street-address": {
                                "@value": "2 Rue Kellermann",
                                "@type": "xsd:string"
                              },
                              "vcard:postal-code": {
                                "@value": "59100",
                                "@type": "xsd:string"
                              },
                              "vcard:locality": {
                                "@value": "Roubaix",
                                "@type": "xsd:string"
                              }
                            },
                            "gax-participant:leiCode": {
                              "@value": "969500Q2MA9VBQ8BG884",
                              "@type": "xsd:string"
                            },
                            "gax-participant:termsAndConditions": {
                              "@type": "gax-core:TermsAndConditions",
                              "gax-core:Value": {
                                "@value": "https://www.ovhcloud.com/fr/terms-and-conditions/",
                                "@type": "xsd:anyURI"
                              },
                              "gax-core:hash": {
                                "@value": "31296ef8727fc63e89213f3e5ab928bae7699b6dade0af5e443b8ad2623639ee8e6176696a1fa125ccfa7fa55465463b6424d3f54435d6a2beafd79a91425b0a",
                                "@type": "xsd:string"
                              }
                            }
                          },
                          "proof": {
                            "type": "JsonWebSignature2020",
                            "proofPurpose": "assertionMethod",
                            "verificationMethod": "did:web:abc-federation.gaia-x.community",
                            "created": "2022-10-20T10:41:00.541875+00:00",
                            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ccBC79pc52G25cXHB-QnWeTfTYthOuEDcnRBxx1tivnIqPj5-QvX3vEOp_iR6H1MS7yBSZOhwPH1loJcXMsrSuiJVhamwMnGlE_vDPQ6hjCc6dqe8xC6g9SG7wdb5Lv5H5dv4k-4GgzOMj5ebtudX9ng1VS_tLiPw9_D6yBwWAD4YVroOor41GAxntIGH1_Wc8oN__aog_eGue5ZrnHFHdu12qzHDhzqi-5AWN0qEELOnsJWU6SVz1JNfyVobzdznCQQemvmzB5TaKjIK-E2Q9AHAI7bi6eVy-PLEIyuDSC3qK6e47kill9myN55znanyXSt0OAjlI5RKrak4-wksg"
                          }
                        }

}

test_checkJsonldValid if {
    valid with input as __inputTest
}