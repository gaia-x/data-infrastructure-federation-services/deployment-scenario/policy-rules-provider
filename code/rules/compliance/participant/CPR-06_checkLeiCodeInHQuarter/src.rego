package rules.compliance.participant.CPR06_checkLeiCodeInHQuarter

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC
import future.keywords.in

ruleSetID := "CPR-06"

default valid := false

valid if {
    isLeiCodeInHQuarterValid(input)
}

isLeiCodeInHQuarterValid(myvc) if {
    leiCode := input["credentialSubject"]["gax-participant:leiCode"]["@value"]
    response := http.send({ "method": "GET", "url": concat("", ["https://api.gleif.org/api/v1/lei-records?filter[lei]=", leiCode])})
    body := json.unmarshal(response["raw_body"])
    leiCountry := body.data[0].attributes.entity.headquartersAddress.country

    country := input["credentialSubject"]["gax-participant:headquarterAddress"]["vcard:country-name"]["@value"]
    result := leiCountry == country
}


