package rules.compliance.participant.CPR04_checkRegNum

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC
import data.rules.compliance.participant.CPR05_checkUSState
import future.keywords.in

ruleSetID := "CPR-04"

default valid := false

valid if {
    isRegNumValid(input)
}

valid if {
    CPR05_checkUSState.valid
}

# TODO API VERIFY NUMBER ISO6523 EUID
isRegNumValid(myvc) if {
    EU := ["AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE", "IS", "NO", "LI", "AUT", "BEL", "BGR", "HRV", "CYP", "CZE", "DNK", "EST", "FIN", "FRA", "DEU", "GRC", "HUN", "IRL", "ITA", "LVA", "LTU", "LUX", "MLT", "NLD", "POL", "PRT", "ROU", "SVK", "SVN", "ESP", "SWE", "040", "056", "100", "191", "196", "203", "208", "233", "246", "250", "276", "300", "348", "372", "380", "428", "440", "442", "470", "528", "616", "620", "642", "703", "705", "724", "752"]
    myvc["credentialSubject"]["gax-participant:legalAddress"]["vcard:country-name"]["@value"] in EU

    #registrationNumber := myvc[credentialSubject][_]["gax-participant:registrationNumber"]["@value"]
}

isOtherLocation(myvc) if {
    
}