package rules.compliance.participant.CPR05_checkUSState

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.VC
import data.rules.compliance.participant.CPR04_checkRegNum
import future.keywords.in

ruleSetID := "CPR-05"

default valid := false

valid if {
    is_VCFromUS(input)
    isUSStateValid(input)
}

valid if {
    CPR04_checkRegNum.isRegNumValid(input)
}


default is_VCFromUS(myvc) := false
is_VCFromUS(myvc) if {
    USA := ["US", "USA", "840"]
    myvc["credentialSubject"]["gx-participant:legalAddress"]["gx-participant:country-name"] in USA
}

default isUSStateValid(myvc) := false
isUSStateValid(myvc) if {
    state := ["AL","AK","AZ","AR","CA","CZ","CO","CT","DE","DC","FL","GA","GU","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","PR","RI","SC","SD","TN","TX","UT","VT","VI","VA","WA","WV","WI","WY"]
    myvc["credentialSubject"]["gax-participant:legalAddress"]["vcard:country-name"]["@value"] in state
}


