package rules.compliance.serviceOffering.CSR01_checkJsonld

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-01"
serviceOffering := verifiable.getServiceOffering

default valid := false
valid if {
    isJsonld(serviceOffering)
}

isJsonld(myvc) if {
    context := "@context"
    myvc[context]
}
