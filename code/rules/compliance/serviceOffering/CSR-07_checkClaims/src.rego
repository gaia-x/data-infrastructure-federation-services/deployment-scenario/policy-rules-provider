package rules.compliance.serviceOffering.CSR07_checkClaims

import future.keywords.if
import future.keywords.every
import data.rules.lib.verifiable
import data.rules.lib.core

ruleSetID := "CSR-07"

default valid := false

valid if {
    true
    #count(thirdPartyClaims) == count(isCredForClaim)
}

thirdPartyClaims := {claim | some it in verifiable.getLSO.credentialSubject["gax-service:hasComplianceCertificateClaim"]; it["@type"] == "gax-compliance:ThirdPartyComplianceCertificateClaim"; claim := it}
claims = {claim | some it in verifiable.getLSO.credentialSubject["gax-service:hasComplianceCertificateClaim"]; claim := it}

complianceReference[complianceReference] {
    some it in thirdPartyClaims
    resFromComplianceCertificateClaim := core.resDid(it["@id"]).body["gax-compliance:hasComplianceCertificationScheme"]["@id"]
    resFromComplianceCertificationScheme := core.resDid(resFromComplianceCertificateClaim).body["gax-compliance:hasComplianceReference"]["@id"]
    some complianceRef in core.resDid(resFromComplianceCertificationScheme).body["gax-compliance:hasComplianceCertificationSchemes"]
    complianceReference := {"didLSO": verifiable.getLSO.credentialSubject["@id"] , "complianceCertificationScheme": complianceRef["@id"], "didClaim": it["@id"]}
}

CAB = {cab | some it in complianceReference; some CCS in core.resDid(it.complianceCertificationScheme).body["gax-compliance:hasComplianceAssessmentBodies"]; cab := {"didLSO": it.didLSO, "CAB": CCS["@id"], "didCCS": it.complianceCertificationScheme, "didClaim": it.didClaim}}

isCredForClaim[res] {
    some it in CAB
    some thirdPartyCCredDid in core.resDid(it.CAB).body["gax-compliance:hasThirdPartyComplianceCertificateCredential"]
    thirdPartyCCred := core.resDid(thirdPartyCCredDid["@id"]).body.credentialSubject
    thirdPartyCCred["gax-compliance:hasComplianceCertificationScheme"]["@id"] == it.didCCS
    thirdPartyCCred["gax-compliance:hasServiceOffering"]["@id"] == it.didLSO
    res := {"credential": thirdPartyCCred["@id"], "claim": it.didClaim}
}