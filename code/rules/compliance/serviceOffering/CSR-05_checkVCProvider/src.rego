package rules.compliance.serviceOffering.CSR05_checkVCProvider

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-05"
serviceOffering := verifiable.getServiceOffering

default valid := false

valid if {
    #isVCProvider
    #Always true, need to change object to VC OR add reference in object to find the linked VC
    true
}

isVCProvider {
    verifiable.isVC(core.resDid(serviceOffering.credentialSubject["gax-service:providedBy"]["@id"]).body)
}