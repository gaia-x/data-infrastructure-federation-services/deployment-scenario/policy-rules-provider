package rules.compliance.serviceOffering.CSR05_checkVCProvider
        
import future.keywords.if

__inputTest := { "vc" : {
                    "@context":[
                       "https://www.w3.org/2018/credentials/#"
                    ],
                    "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/4429512748ebd2924cf50e2b3efee377e76038e346a4d3ad266d47dfcf1aa5df/data.json",
                    "@type":[
                       "VerifiablePresentation"
                    ],
                    "verifiableCredential":[
                       {
                          "@context":[
                             "https://www.w3.org/2018/credentials/#"
                          ],
                          "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/5d61abb4ff29f50abbf4d21e0c3696dd5b2693fe3deaae3275f2d500ecc8b5c1/data.json",
                          "issuer":"did:web:dufourstorage.provider.gaia-x.community",
                          "@type":[
                                "VerifiableCredential"
                          ],
                          "credentialSubject":{
                             "@context": {
                                "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
                             },
                             "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/located-service-offering/9e9caae3a13c28cad5371c3703705688191d0a082e33ef87d8a79fff9acb8760/data.json",
                             "@type":"gax-service:LocatedServiceOffering",
                             "gax-service:isImplementationOf":{
                                "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/service-offering/16ea3d9c5c9ef155dfee355366b44fc7119afdbeb84b809d54518b253d67310d/data.json",
                                "@type": "gax-service:ServiceOffering"
                             },
                             "gax-service:isHostedOn":{
                                "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/location/f17220a5c93bdc04451b1253015b4e2c2dc1cd532b9e4e6e75f13f39080024f6/data.json",
                                "@type": "gax-service:Location"
                             },
                             "gax-compliance:hasComplianceCertificateClaim":[
                                {
                                   "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/3510267eaf591f87b3d217a4348a2d26d374b6bfb9b82e2d4764192196cb6b6b/data.json",
                                   "@type":"gax-compliance:ComplianceCertificateClaim"
                                },
                                {
                                   "@id":"did:web:ovhcloud.provider.gaia-x.community:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/compliance-certificate-claim/510267eaf591f87b3d217a4348a2d26d374b6bfb9b82e2d4764192196cb6b6b3/data.json",
                                   "@type":"gax-compliance:ComplianceCertificateClaim"
                                }
                             ]
                          }
                       }
                    ],
                    "proof":{
                       "type":"JsonWebSignature2020",
                       "proofPurpose":"assertionMethod",
                       "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
                       "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..LQZbnJQq-Jh75N33D8MXFgiFPvvw1EFK5FuUXbsOgQfy9U_EEGxQ9D2LgnWIhTRWOBQl3IYvanH-2ggGlg3YhQlQpCedgc1XWci-E_a5tjZAyjg-oKqN265yqjw3xbMvfHUfcHyR-F4FgYANIxQNixHvv9Kuj-6sjAANi-KwLZnyDVrTxZ0jBS4VJoIXuGeHBETaE7mciiinBZjvbRjOv76MHEQJnzf4YcOngcKQtaSGYB5dXc4hxe7fEpe4q3kSeO2xmuCwwSMjpgijoTnIrYPMej9i3lnkjUusWIbkbQch4Bk2C9MCsxGR-wDUIey5VbVVP9eru3hpiEO4Qi2eig"
                    }
                 }
}

test_checkHttpCode if {
    valid with input as __inputTest
}