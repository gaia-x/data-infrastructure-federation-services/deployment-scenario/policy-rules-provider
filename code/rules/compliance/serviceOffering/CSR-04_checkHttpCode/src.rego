package rules.compliance.serviceOffering.CSR04_checkHttpCode

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-04"
serviceOffering := verifiable.getServiceOffering

default valid := false

valid if {
    all_URIValid(serviceOffering)
}

all_URIValid(myVC) {
    not any_not_URIValid(myVC)
}

regExpURI := "^https?://(www.)?[-a-zA-Z0-9@:%._+~#=]{1,256}.[a-zA-Z0-9()]{1,6}"

any_not_URIValid(myvc) if {

   count(lstNonValidURIinVC(myvc)) > 0
}

#vérification du redirecte
sucessfulHTTPCode(code) {code >= 200; code <=307} #TODO Only 2XX verification, needs to verifie redirected link
redirecteHTTPCode(code) {code >= 300; code <=307}

lstURIinVC(myvc) := { uri | [path, value] := walk(myvc) ;uri := value[_]; regex.match(regExpURI, uri)}
lstValidURIinVC(myvc) := { uri | response := http.send({ "method": "GET", "url": lstURIinVC(myvc)[uri]});  sucessfulHTTPCode(response.status_code)  }
lstNonValidURIinVC(myvc) := lstURIinVC(myvc) - lstValidURIinVC(myvc)
lstRedirectedURIinVC(myvc) := { uri | response := http.send({ "method": "GET", "url": lstNonValidURIinVC(myvc)[uri]});  redirecteHTTPCode(response.status_code)  }

lstURI := lstURIinVC(serviceOffering)
lstValidURI := lstValidURIinVC(serviceOffering)
lstNonValidURI := lstURI - lstValidURI
lstRedirectedURI := lstRedirectedURIinVC(serviceOffering)