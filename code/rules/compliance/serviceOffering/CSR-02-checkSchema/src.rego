package rules.compliance.serviceOffering.CSR02_checkSchema

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-02"

default valid := false

valid if {
    isSchemaValid
}

isSchemaValid {
    verifiable.isVP(input)
    response := http.send({ "method": "POST", "url": "https://schemavalidator.abc-federation.gaia-x.community/locatedServiceOffering", "body": input});
    response.status_code == 200
}