package rules.compliance.serviceOffering.CSR06_checkDid

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-06"
serviceOffering := verifiable.getServiceOffering

default valid := false

valid if {
    not isDIDOk(serviceOffering)
}

regexDID := "^did:web:.*"

isDIDOk(myvc) if {
    count(lstNonValidDIDinVC(myvc)) > 0
}

lstDIDinVC(myvc) := { did | [path, value] := walk(myvc) ;didURL := value[_]; regex.match(regexDID, didURL); did := split(didURL, "/")[0]}
lstValidDIDinVC(myvc) := { did | response := http.send({ "method": "GET", "url": concat("", ["http://resolver.lab.gaia-x.eu:8080/1.0/identifiers/", trim_suffix(lstDIDinVC(myvc)[did], "/data.json")])}); response.status_code == 200}
lstNonValidDIDinVC(myvc) := lstDIDinVC(myvc) - lstValidDIDinVC(myvc)

lstDID := lstDIDinVC(serviceOffering)
lstValidDID := lstValidDIDinVC(serviceOffering)
lstNonValidDID := lstDID - lstValidDID
