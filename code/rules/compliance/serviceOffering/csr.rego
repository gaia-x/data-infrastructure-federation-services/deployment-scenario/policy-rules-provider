package rules.compliance.complianceServiceOffering

import future.keywords.in
import data.rules.lib.VPLSO

anyNotValid {
    data.rules.compliance.serviceOffering[_].valid == false
}

default isValid := false
isValid {
    not anyNotValid
}

lstNonValid := { rule | data.rules.compliance.serviceOffering[rule].valid == false}
lstValid := { rule | data.rules.compliance.serviceOffering[rule].valid == true}

valid := res {
    isValid
    res := {"valid": "true",
            "VC": {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1",
                        {
                            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        }
                      ],
                    "@id": "",
                    "@type": [
                      "VerifiableCredential"
                    ],
                    "issuer": "",
                    "expirationDate": "",
                    "credentialSubject": {
                      "@type": "gax-compliance:ComplianceObject",
                      "targetType" : "LocatedServiceOffering",
                      "vc": vc,
                      "validRules": lstValid,
                      "vcProof": vcProof
                    },
                    "evidence": {
                      "verifier": "",
                      "type": [],
                      "subjectPresent": "digital",
                      "documentPresent": "digital"
                    }
                  }}
}

valid := res {
    not isValid
    res := {"valid": "false", "lstNonValid": lstNonValid}
}

vc := res {
    res := VPLSO.getLocatedServiceVC.credentialSubject["gax-service:isImplementationOf"]["@id"]
}

vc := res {
    res := VPLSO.getLocatedServiceVC.credentialSubject["gax-service:isImplementationOf"].id
}

vcProof := res {
    res := VPLSO.getLocatedServiceVC.proof
}