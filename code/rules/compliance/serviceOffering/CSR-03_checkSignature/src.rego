package rules.compliance.serviceOffering.CSR03_checkSignature

import future.keywords.if
import data.rules.lib.core
import data.rules.lib.verifiable

ruleSetID := "CSR-03"
#serviceOffering := verifiable.getServiceOffering

default valid := false

valid if {
    isValid(input)
    #Remove until new valid DIDs are created + add multiple proof vérification
    #true
}

isValid(myvc) {
    #VC
    # Normalize it with compliance lab service rest api and hash it
    payload := verifiable.payload(myvc)

    #JWT
    # Reconstruct jwt with header + signature from jws and add payload
    # Data : JWS = Header . . Signature
    #        JWT = Header . Payload . Signature
    
    header_signature := split(myvc.proof.jws, "..")
    jwt := concat(".", [header_signature[0], payload, header_signature[1]])

    #JWK
    # Construct key structure wrt OPA built-in functions
    keys := { "keys" : [key] |
        jwk := verifiable.getPubKey(myvc)
        key := jwk["verificationMethod"][_]["publicKeyJwk"]
    }

    #Verify signature
    proof := io.jwt.verify_ps256(jwt, json.marshal(keys))
}