package rules.compliance.complianceServiceOffering

import future.keywords.if
import data.rules.lib.core

__inputTest := {
                   "@context": [
                       "https://www.w3.org/2018/credentials/v1"
                   ],
                   "type": [
                       "VerifiableCredential",
                       "LegalPerson"
                   ],
                   "id": "https://abc-federation.gxfs.fr/public-data/participant.json",
                   "issuer": "did:web:abc-federation.gxfs.fr",
                   "issuanceDate": "2022-09-23T23:23:23.235Z",
                   "credentialSubject": {
                       "id": "did:web:abc-federation.gxfs.fr/public-data/participant.json",
                       "type": "gx-participant:Participant",
                       "gx-participant:name": "Fictive federation using Gaia-X federation services",
                       "gx-participant:legalName": "ABC federation",
                       "gx-participant:registrationNumber": {
                           "gx-participant:registrationNumberType": "local",
                           "gx-participant:registrationNumberNumber": "0762747721"
                       },
                       "gx-participant:headquarterAddress": {
                           "gx-participant:addressCountryCode": "BE",
                           "gx-participant:addressCode": "BE-BRU",
                           "gx-participant:street-address": "Avenue des Arts 6-9",
                           "gx-participant:postal-code": "1210"
                       },
                       "gx-participant:legalAddress": {
                           "gx-participant:addressCountryCode": "BE",
                           "gx-participant:addressCode": "BE-BRU",
                           "gx-participant:street-address": "Avenue des Arts 6-9",
                           "gx-participant:postal-code": "1210"
                       },
                       "gx-participant:termsAndConditions": "70c1d713215f95191a11d38fe2341faed27d19e083917bc8732ca4fea4976700"
                   },
                   "proof": {
                       "type": "JsonWebSignature2020",
                       "proofPurpose": "assertionMethod",
                       "verificationMethod": "did:web:abc-federation.gxfs.fr",
                       "created": "2022-10-14T13:20:14.132602+00:00",
                       "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..RyLKuE7eevKMDaw-oEVAFcI5bJ5gd6Cs2sGEJS6rmEOnH0rY3r8qICqhD4702TqLsa5S1Lv45hfNbk4Zs4qYfvu4zJ0pRe17-gETsD5Yp7kTFVxkQsVvs1n3mxz3ClDmW-UDKzVGZSqGTwmH0kDbK4VIjHESkTbVtkuWRnkC6XVksqJVicnqosdc8XyCrwBCy0VjOVAiyuXyosbScsvlqW1dRQ8PcqYPjYWkpPBmfJ7GWE16uodbm6nhgdsLjn634SJv6n13lnTJ6ZA-t1019_gvnufCkC9VLaU8IWFB0HEwvh-NQg6t5594dFGFGJtQVhgxX_--GHtIXqSWZ3xaeg"
                   }
               }

test_checkCSR if {
    not valid with input as __inputTest
}