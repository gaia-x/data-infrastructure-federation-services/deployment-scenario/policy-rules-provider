#!/bin/sh

ip=162.19.23.15
#ip=localhost:8181

echo "Need curl and jq to be installed !"
while true; do
    read -p "Test rules with opa server ip = $ip? [y/n]" yn
    case $yn in
        [Yy]* ) echo "let's go"; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "(1/2) VerifyDataProof Rule Testing..."
curl -s POST http://$ip/v1/data/rules/verifydataproof/policy | jq

echo "(2/2) VerifyProof Rule Testing..."
echo '{"input":{"vc":' $(cat rules/vcs/vc1/data.json) '}}' > test.json
curl -s POST http://$ip/v1/data/rules/verifyproof/policy -d @test.json | jq
rm test.json
