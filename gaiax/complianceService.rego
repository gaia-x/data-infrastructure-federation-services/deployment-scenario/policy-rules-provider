# METADATA
# title: check URI validity as defiend by W3C
# description: 
#   
# custom:
#   

package gaiax

import future.keywords.if
import future.keywords.every
import data.gaiax.lib.core
import data.gaiax.lib.VC

default valid := false
default allServiceValid := false


allServiceValid if {
    every rule in data.gaiax.compliance.service {rule.valid}
}

anyNotValid {
    data.gaiax.compliance.service[_].valid == false
}

valid { not anyNotValid}

lstserviceNonValid := { rule | data.gaiax.compliance.service[rule].valid == false}
