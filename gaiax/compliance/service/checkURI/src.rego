# METADATA
# title: check URI validity as defiend by W3C
# description: 
#   
# custom:
#   

package gaiax.compliance.service.checkURI

import future.keywords.if
import data.gaiax.lib.core
import data.gaiax.lib.VC

ruleSetID := "CSR-04"

default valid := false

valid if {
    all_URLValid(VC.resource)
}

all_URLValid(myVC) {
    not any_not_URLValid(myVC)
}

regExpURI := "https?://(www.)?[-a-zA-Z0-9@:%._+~#=]{1,256}.[a-zA-Z0-9()]{1,6}"

any_not_URLValid(myvc) if {
    
   count(lstNonValidURIinVC(myvc)) > 0
}

sucessfulHTTPCode(code) {code >= 200; code <300}

lstURIinVC(myvc) := { uri | [path, value] := walk(myvc) ;uri := value[_]; regex.match(regExpURI, uri)}
lstValidURLinVC(myvc) := { uri | response := http.send({ "method": "GET", "url": lstURIinVC(myvc)[uri]});  sucessfulHTTPCode(response.status_code)  } 
lstNonValidURIinVC(myvc) := lstURIinVC(myvc) - lstValidURLinVC(myvc) 

lstURI := lstURIinVC(VC.resource)
lstValidURI := lstValidURLinVC(VC.resource)
lstNonValidURI := lstURI - lstValidURI

violation[msg] {
	myvc=VC.resource
	any_not_URLValid(myvc)
	msg := core.format_with_id(sprintf("%s: Any URL in VC is not valid", [myvc.id]), ruleSetID)
}


