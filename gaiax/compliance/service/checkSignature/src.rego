# METADATA
# title: check signatures validity as defiend by W3C
# description: 
#   
# custom:
#   

package gaiax.compliance.service.checkSignature

import future.keywords.if
import data.gaiax.lib.core
import data.gaiax.lib.VC

ruleSetID := "CSR-03"

default valid := false

valid if {
    isValid(VC.resource)
}

isValid(myvc) := proof if {
    #VC
    # Normalize it with compliance lab service rest api and hash it
    payload := VC.payload(myvc)

    #JWT
    # Reconstruct jwt with header + signature from jws and add payload
    # Data : JWS = Header . . Signature
    #        JWT = Header . Payload . Signature
    
    header_signature := split(myvc.proof.jws, "..")
    jwt := concat(".", [header_signature[0], payload, header_signature[1]])

    #JWK
    # Construct key structure wrt OPA built-in functions
    keys := { "keys" : [key] |
        jwk := VC.getPubKey(myvc)
        key := jwk["verificationMethod"][_]["publicKeyJwk"]
    }

    #Verify signature
    proof := io.jwt.verify_ps256(jwt, json.marshal(keys))
}

violation[msg] {
	myvc=VC.resource
	not isValid(myvc)
	msg := core.format_with_id(sprintf("%s: Signature of VC is not valid", [myvc.id]), ruleSetID)
}






