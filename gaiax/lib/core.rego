package gaiax.lib.core

format(msg) := obj {obj :=  {"msg": msg} }

format_with_id(msg, id) := msg_fmt {
	msg_fmt := {
		"msg": sprintf("%s: %s", [id, msg]),
		"details": {"policyID": id}
	}
}


has_field(obj, field) {
	not object.get(obj, field, "N_DEFINED") == "N_DEFINED"
}

missing_field(obj, field) {
	obj[field] == ""
}

missing_field(obj, field) {
	not has_field(obj, field)
}
