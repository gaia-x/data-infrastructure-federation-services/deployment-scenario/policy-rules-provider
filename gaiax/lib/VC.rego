package gaiax.lib.VC

import future.keywords.if

import data.gaiax.lib.core

url_normalize := "https://compliance.gaia-x.eu/v2204/api/normalize"

default is_VC := false
default is_VCincludedinInput = false

is_VCincludedinInput if {
    core.has_field(input,"vc")
    core.has_field(input.vc,"@type")
}

resource = input.vc if {
	is_VCincludedinInput
}

resource = input if {
	not is_VCincludedinInput
}


is_VC if {
    core.has_field(resource,"@type")
    core.has_field(resource,"proof")
    resource["@type"][_] == "VerifiableCredential"
}


normalize(vc) = vcnp if {
    vc_no_proof := json.remove(vc, ["proof"])
    #Normalize it with compliance lab service rest api
    return := http.send({"method": "POST", "url": url_normalize , "headers": {"Content-Type": "application/json"}, "body": vc_no_proof })
    #Hash normalization to get paylaod
    vcnp := return.raw_body
}

payload(vc) = payloadvc if {
    vcnp_normalized := normalize(vc)
    payloadvc := crypto.sha256(vcnp_normalized)
}


getPubKey(vc) = key if {
    #Get domain name from proof of vc
    vf := vc["proof"]["verificationMethod"]
    dn := trim(vf, "did:web:")
    url := concat("", ["https://", dn, "/.well-known/did.json"])
    #Call vc issuer service to get its pub key
    return := http.send({"method": "GET", "url": url, "headers": {"Content-Type": "application/json"} })
    key := json.unmarshal(return["raw_body"])
}
